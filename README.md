You can install SpringToolShuit in below url.
https://spring.io/tools

You should clone git from url(https://gitlab.com/claretta/crawlinghtml.git)
or SSH(git@gitlab.com:claretta/crawlinghtml.git)

Start STS and then import maven project in Git Repository.
Right click on imported project, and click Run as -> Maven Build.
Write "clean package" on the Goal, check "skip test".
After Finishing build, run the project on the Boot DashBoard.

You can access local environment with url http://localhost:8080
