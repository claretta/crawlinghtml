package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.data.InputData;
import com.example.demo.data.OutputData;
import com.example.demo.data.ResponseStatus;
import com.example.demo.service.CrawlingService;

@RestController
public class CrawlingController {

	private final CrawlingService crawlingService;
	
	public CrawlingController(CrawlingService crawlingService) {
		this.crawlingService = crawlingService;
	}
	
	@GetMapping("/rest/crawlhtml")
	public OutputData crawlHtml(InputData inputData) throws Exception {
		OutputData outputData = new OutputData();
		try {
			outputData = crawlingService.crawlHtml(inputData);			
		} catch(Exception e) {
			return new OutputData(ResponseStatus.ERROR);
		} 
		return outputData;
	}	
}
