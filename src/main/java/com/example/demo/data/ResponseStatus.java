package com.example.demo.data;

public enum ResponseStatus {
	SUCCESS(200, "successs"),
	BAD_REQUEST(400, "잘못된 요청입니다."),
	ERROR(900, "처리 중 오류가 발생했습니다."),
	NO_URL(901, "url은 필수값입니다."),
	URL_FORMAT(902, "url이 형식에 맞지 않습니다."),
	NO_HTML(903, "해당 url에 html 문자열이 존재하지 않습니다."),
	UNIT_RANGE(904, "출력 단위 묶음은 0보다 큰 값이어야 합니다."),
	FAIL_CONNECT(905, "해당 주소로의 연결에 실패했습니다."),
	;
	
	private int code;
	private String message;
		
	ResponseStatus(int code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public int getCode() {
		return this.code;
	}
	
	public String getMessage() {
		return this.message;
	}	
}
