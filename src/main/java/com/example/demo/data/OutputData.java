package com.example.demo.data;

public class OutputData {	
	private String share;
	private String remainder;
	private int code;
	private String message;
	
	public OutputData() {}	
	
	public OutputData(String share, String remainder) {
		this.share = share;
		this.remainder = remainder;
	}	
	
	public OutputData(ResponseStatus responseStatus) {
		this.code = responseStatus.getCode();
		this.message = responseStatus.getMessage();
	}
	
	public String getShare() {
		return this.share;
	}
	
	public String getRemainder() {
		return this.remainder;
	}	
	
	public int getCode() {
		return this.code;
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public void setShare(String share) {
		this.share = share;
	}
	
	public void setRemainder(String remainder) {
		this.remainder = remainder;
	}
	
	public void setResponseStatus(ResponseStatus responseStatus) {
		this.code = responseStatus.getCode();
		this.message = responseStatus.getMessage();
	}
}
