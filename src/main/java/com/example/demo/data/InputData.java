package com.example.demo.data;

public class InputData {
	private String url;
	private String type;
	private int outputUnit;
	
	public String getUrl() {
		return this.url;
	}
	
	public String getType() {
		return this.type;
	}
	
	public int getOutputUnit() {
		return this.outputUnit;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public void setOutputUnit(int outputUnit) {
		this.outputUnit = outputUnit;
	}
	
	@Override
	public String toString() {
		return "inputData{" + "url='" + url + "\', type=" + type + "\', outputUnit = " + outputUnit + "}";
	}
}
