package com.example.demo.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import com.example.demo.data.InputData;
import com.example.demo.data.OutputData;
import com.example.demo.data.ResponseStatus;

@Service
public class CrawlingService {

	/**
	 * 입력받은 url을 통해 조건에 맞게 html 코드를 파싱하여 정렬 및 나누는 메서드
	 * @param inputData
	 * @return OutputData
	 * @throws Exception
	 */
	public OutputData crawlHtml(InputData inputData) throws Exception {
		if(inputData == null) {
			return new OutputData(ResponseStatus.BAD_REQUEST);
		}
		
		String url = inputData.getUrl();
		
		if(url.isBlank()) 
			return new OutputData(ResponseStatus.NO_URL);
		
		String html = null;
		
		//url Crawling
		try {
			html = crawlUrl(url);
		} catch(MalformedURLException me) {
			System.out.println("me");
			return new OutputData(ResponseStatus.URL_FORMAT);
		} catch(IOException ie) {
			System.out.println("ie");
			return new OutputData(ResponseStatus.FAIL_CONNECT);
		}		
		
		if(html == null)
			return new OutputData(ResponseStatus.NO_HTML);
		
		if(inputData.getType().equals("exceptTag"))
			html = removeTag(html);
		
		String letters = classifyLetter(html);
		System.out.println("영어만 출력 : " + letters);
		
		int[] numbers = classifyNumber(html);
		System.out.print("숫자만 출력 : ");
		for(int idx = 0; idx < numbers.length; idx++) {
			System.out.print(numbers[idx]);
		}
		System.out.println();
		
		System.out.println("오름차순 출력");
		letters = sortLetter(letters);
		System.out.println(" - 영어 : " + letters);
		String strNumbers = sortNumber(numbers);
		System.out.println(" - 숫자 : " + strNumbers);
		
		String mixed = mixLetterNumber(letters, strNumbers);
		System.out.println("교차출력 : " + mixed);
		
		int outputUnit = inputData.getOutputUnit();
		
		if(outputUnit <= 0)
			return new OutputData(ResponseStatus.UNIT_RANGE);
		
		OutputData outputData = calOutputUnit(mixed, outputUnit);
		
		outputData.setResponseStatus(ResponseStatus.SUCCESS);
		
		return outputData;
	}

	/**
	 * url로 connection을 생성하여 html 코드를 파싱하는 메서드
	 * @param url
	 * @return html
	 * @throws Exception
	 */
	private String crawlUrl(String url) throws Exception {
				
        // 1. 수집 대상 URL
		if(!url.contains("http://") && !url.contains("https://")){
			url = "http://" + url;
		}
            
        // 2. Connection 생성
        Connection conn = Jsoup.connect(url);
 
        // 3. HTML 파싱
        Document html = conn.get();
            
        return html.toString();
	}
	
	/**
	 * type에 따라, html 코드의 tag를 제거하는 메서드
	 * @param target
	 * @return 
	 */
	private String removeTag(String target) {
		return target.replaceAll("\\<.*?\\>", "");
	}
	
	/**
	 * String에서 알파벳만 분류하는 메서드
	 * @param target
	 * @return result
	 */
	private String classifyLetter(String target) {
		Pattern nonValidPattern = Pattern.compile("[a-zA-Z]");

		Matcher matcher = nonValidPattern.matcher(target);
		StringBuilder result = new StringBuilder(""); 
		  
		while (matcher.find()) {
		    result.append(matcher.group()); 
		}

		return result.toString();		
	}
	
	/**
	 * 알파벳을 정렬하는 메서드
	 * @param target
	 * @return 
	 */
	private String sortLetter(String target) {		
		String[] strArray = target.split("");
	    Arrays.sort(strArray, (a, b) -> compare(a,b));
		
		return String.join("",strArray);
	}
	
	/**
	 * 알파벳 비교 메서드(대문자를 우선으로 알파벳 순)
	 * @param target
	 * @param source
	 * @return
	 */
	private int compare(String target, String source) {
		if(target.compareToIgnoreCase(source) == 0) {
			return (int)target.charAt(0) - (int)source.charAt(0);
		}else {
			return target.toLowerCase().compareTo(source.toLowerCase());
		}
	}
	
	/**
	 * String에서 숫자만을 분류해서 int 배열로 반환하는 메서드
	 * @param target
	 * @return
	 */
	private int[] classifyNumber(String target) {
		IntStream stream = target.chars();
		int[] result = stream.filter((ch)-> (48 <= ch && ch <= 57))
		        .mapToObj(ch -> (char)ch)
		        .mapToInt(ch -> (int)ch - '0')
		        .toArray();
		return result;
	}
	
	/**
	 * int 배열을 정렬하여 String으로 반환하는 메서드
	 * @param target
	 * @return
	 */
	private String sortNumber(int[] target) {
		Arrays.sort(target);
		
		return Arrays.stream(target)
				.mapToObj(num -> Integer.toString(num))
				.collect(Collectors.joining());
	}
	
	/**
	 * 문자와 숫자를 하나씩 섞는 메서드
	 * @param letters
	 * @param numbers
	 * @return mix
	 */
	private String mixLetterNumber(String letters, String numbers) {
		StringBuffer mix = new StringBuffer("");
		int numIdx = 0;
		int letIdx = 0;
		do {
			if(letters.length() > letIdx) {
				mix.append(letters.substring(letIdx, letIdx+1));
				letIdx++;
			}
			if(numbers.length() > numIdx) {
				mix.append(numbers.substring(numIdx, numIdx+1));
				numIdx++;
			}
		} while(letters.length() > letIdx || numbers.length() > numIdx);
		
		return mix.toString();
	}
	
	/**
	 * String을 outputUnit에 따라 나눈 몫과 나머지를 구하는 메서드
	 * @param target
	 * @param outputUnit
	 * @return outputData
	 */
	private OutputData calOutputUnit(String target, int outputUnit) {
		
		int remainNum = target.length() % outputUnit;
		
		String share = target.substring(0, target.length() - remainNum);
		String remainder = target.substring(target.length() - remainNum);
		
		return new OutputData(share, remainder);
	}
}
